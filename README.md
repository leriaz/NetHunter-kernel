## EDIT: Old as shit. You may still give it a shot. Everything below applies to SDK 23, and only this one. Might be hard to find some of the dependencies nowdays. 




<h2 align="center">Kali NetHunter port for the LG G3(d855)</h2>

#### Note:
Flashing custom roms/kernels etc. you always run the risk of bricking/soft-bricking your device. Same risk is applied here.
Continue only if you know what you're doing.


#### Pre-installation requirements:
- LG G3 (d855) **rooted**
- [CyanogenMod](https://download.cyanogenmod.org/?device=d855) (Tested on 20160914-NIGHTLY, I see no reason it wouldn't work on the latest nightly)
- [TWRP Recovery](https://twrp.me/devices/lgg3europe.html) (Tested on 2.8.6.0, it should work on the latest also)
- Developer Options enabled and activated root
- Download [NetHunter Release](https://gitlab.com/leriaz/NetHunter-kernel/tags/1.0.0) and [MR_Bump](https://mirrors.c0urier.net/android/Dadi11/Required%20files/MR_Bump.zip)

#### Installation (assuming you already have CyanogenMod and TWRP installed and activated root):
- Get the downloaded files on your phone.
- Reboot in TWRP recovery.
- Flash NetHunter.
- Flash MR_Bump
- Done.

#### Post-Installation actions:
- Run the NetHunter app and select a chroot installation.
- Then you may select a Metapackage if you wish. More info on them [here](https://www.kali.org/news/kali-linux-metapackages/)

#### Known issues:
- If the terminal force closes remove chroot and chroot your device again.
- If it fails again, you should re-install BusyBox(or install for the first time if you haven't already.. )
- When using an external wifi device, the moment you unplug it your phone reboots. Until this issue is fixed, whenever you want to remove the external device, first go into the terminal and type 'iw dev wlan1 interface del' -- wlan1 may be wlan0 or anything depending on your configuration.
- HID MITM throws errors at Kernel compilation time. This will be fixed soon. Every other NetHunter tool works perfectly.

#### A few personal suggestions:
- [Termux](https://f-droid.org/repository/browse/?fdfilter=termux&fdid=com.termux) instead of the NetHunter terminal*
- [Hacker's keyboard](https://f-droid.org/repository/browse/?fdfilter=hacker&fdid=org.pocketworkstation.pckeyboard)


\* *Note: When using a terminal application other than NetHunter's, in order to boot into Kali, you need to run ``` bootkali ``` on an elevated shell*
